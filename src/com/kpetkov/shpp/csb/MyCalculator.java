package com.kpetkov.shpp.csb;

import java.util.HashMap;

/**
 * Created by KOSS on 26.05.2017.
 */
public class MyCalculator {
    private KPLinkedList<Double> operands = new KPLinkedList<>();    // List of operands
    private KPLinkedList<Character> operators = new KPLinkedList<>();// List of operators
    private HashMap<Character, String> variables = new HashMap<>();  // Variable map

    /**
     * The method determines whether the character is an operator
     *
     * @param symbol - next character in the string
     * @return - if the operator is true
     */
    private boolean isOperator(Character symbol) {
        return symbol == '-' || symbol == '+' || symbol == '*' || symbol == '/';
    }

    /**
     * Determines operator priority
     *
     * @param operator - symbol operator
     * @return - returns an integer equal to the operator's priority
     */
    private int priority(Character operator) {
        if (operator == '*' || operator == '/') {
            return 1;
        } else if (operator == '+' || operator == '-') {
            return 0;
        } else {
            return -1;
        }
    }

    /**
     * Checks operator minus or part of number
     *
     * @param index   - character index in a string
     * @param formula - processing string
     * @return - If the operator false, if the sign true.
     */
    private boolean isBinari(Integer index, String formula) {
        if ((index == 0) || (formula.charAt(index - 1) == '(') || isOperator(formula.charAt(index - 1)) || (formula.charAt(index - 1) == ' ') || formula.charAt(index - 1) == '=') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Accepts the values of the linked list and the operator symbol.
     * Two variables take the last values of the linked list and delete them.
     * Performing mathematical operations depending on the operator.
     *
     * @param list     - list of operands
     * @param operator - symbol operator
     */
    private void mathActions(KPLinkedList<Double> list, Character operator) {
        double secondOperand = list.removeLast();
        double firstOperand = list.removeLast();

        switch (operator) {
            case '+':
                list.addLast(firstOperand + secondOperand);
                break;
            case '-':
                list.addLast(firstOperand - secondOperand);
                break;
            case '*':
                list.addLast(firstOperand * secondOperand);
                break;
            case '/':
                list.addLast(firstOperand / secondOperand);
                break;
        }
    }

    /**
     * Each character in the line is checked in a loop.
     * Depending on the value with it, various actions are performed
     *
     * @param formula - string for parsing
     * @return - result of the mathematical expression
     */
    public double resultOfExpression(String formula) {
        for (Integer i = 0; i < formula.length(); i++) {
            Character symbolForParsing = formula.charAt(i);
            if (symbolForParsing == '(') {
                operators.addLast('(');
            } else if (symbolForParsing == ')') {
                while (operators.getLast() != '(') {
                    mathActions(operands, operators.removeLast());
                }
                operators.removeLast();
            } else if (isOperator(symbolForParsing)) {
                i = parseSign(symbolForParsing, i, formula);
            } else if (Character.isLetter(symbolForParsing)) {
                i = parseLetter(formula, i);
            } else if (Character.isDigit(formula.charAt(i))) {
                i = parseDigit(formula, i);
            }
        }
        while (!operators.isEmpty()) {
            mathActions(operands, operators.removeLast());
        }
        return operands.getFirst();
    }

    /**
     * The method takes the name of the function and its argument.
     * Returns the value of class functions Math.java
     *
     * @param fun   - name of the function
     * @param value - argument
     * @return - value
     */
    private double mathFunc(String fun, String value) {
        if (fun.equals("sin")) {
            return Math.sin(Double.parseDouble(value));
        } else if (fun.equals("cos")) {
            return Math.cos(Double.parseDouble(value));
        } else if (fun.equals("sqrt")) {
            return Math.sqrt(Double.parseDouble(value));
        }
        return 0;
    }


    /**
     * If the sign refers to a negative number, then add it to the list.
     * If the operator, then perform the calculation
     *
     * @param symbol  - symbol for parsing
     * @param index   - index simbol in string
     * @param formula - string for parsing
     * @return - post-parse string index
     */
    private Integer parseSign(Character symbol, Integer index, String formula) {
        if ((symbol == '-') && isBinari(index, formula)) {
            String oper = "-";
            index++;
            while (index < formula.length() && (Character.isDigit(formula.charAt(index)) || formula.charAt(index) == '.')) {
                oper += formula.charAt(index++);
            }
            --index;
            operands.addLast(Double.parseDouble(oper));
        } else {
            while (!operators.isEmpty() && priority(operators.getLast()) >= priority(symbol)) {
                mathActions(operands, operators.removeLast());
            }
            operators.addLast(symbol);

        }
        return index;
    }


    /**
     * If letter, then either a variable or a function.
     * If the variable, write its value on the map or add a value to the list.
     * If the function, then calculate the value and write to the list.
     *
     * @param formula - string for parse
     * @param index   - index symbol in string
     * @return - post-parse string index
     */
    private Integer parseLetter(String formula, Integer index) {
        char ch = formula.charAt(index);
        if (formula.charAt(index + 1) == '=') {
            index += 2;
            if (formula.charAt(index) == '-') {
                String value = "-";
                index++;
                while (Character.isDigit(formula.charAt(index)) || (formula.charAt(index) == '.')) {
                    value += formula.charAt(index);
                    index++;
                }
                variables.put(ch, value);
            } else {
                String value = "";
                while (Character.isDigit(formula.charAt(index)) || (formula.charAt(index) == '.')) {
                    value += formula.charAt(index);
                    index++;
                }
                variables.put(ch, value);
            }
        } else if (variables.containsKey(ch)) {
            operands.addLast(Double.parseDouble(variables.get(ch)));
        } else if (Character.isLetter(formula.charAt(index + 1))) {
            index = parseFunc(formula, index);
        }
        return index;
    }


    /**
     * If the number is, then write to the list
     *
     * @param formula - string for parsing
     * @param index   - index symbol in string
     * @return - post-parse string index
     */
    private Integer parseDigit(String formula, Integer index) {
        String operand = "";
        while (index < formula.length() && (Character.isDigit(formula.charAt(index)) || formula.charAt(index) == '.')) {
            operand += formula.charAt(index++);
        }
        --index;
        operands.addLast(Double.parseDouble(operand));
        return index;
    }


    /**
     * Finds a function on the map and calculates its value.
     * Received value for the entry in the list.
     *
     * @param formula - string for parsing
     * @param index   - index symbol in string
     * @return - post-parse string index
     */
    private Integer parseFunc(String formula, Integer index) {
        String funck = "";
        while (formula.charAt(index) != '(') {
            funck += formula.charAt(index);
            index++;
        }
        index++;
        String val = "";
        char letter = formula.charAt(index);
        if (Character.isLetter(letter)) {
            if (variables.containsKey(letter)) {
                operands.addLast(mathFunc(funck, variables.get(letter)));
                index += 1;
            }
        } else {
            while (formula.charAt(index) != ')') {
                val += formula.charAt(index);
                index++;
            }
            operands.addLast(mathFunc(funck, val));
        }
        return index;
    }
}
