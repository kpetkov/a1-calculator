package com.kpetkov.shpp.csb;

/**
 * Created by KOSS on 13.08.2017.
 */
public class Main {
    public static void main(String[] args) {
        String formula = "";
        if (args.length > 0) {
            for (int i = 0; i < args.length; i++) {
                formula += args[i];
            }
        } else {
            formula = args[0];
        }
        MyCalculator myCalculator = new MyCalculator();
        System.out.println("result = " + myCalculator.resultOfExpression(formula));

    }
}
